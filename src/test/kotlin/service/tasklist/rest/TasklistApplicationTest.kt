package service.tasklist.rest

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType.APPLICATION_JSON_UTF8
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import service.tasklist.domain.Tasklist

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class TasklistApplicationTest {

	@Autowired
	lateinit var mvc: MockMvc

	@Autowired
	lateinit var repository: service.tasklist.domain.TasklistRepository

    @After
    fun tearDown() {
        repository.deleteAll()
    }

    @Test
	fun thatInitialTasklistIsCreated() {
		mvc.perform(get("/tasklists")
				.with(user("test1@localhost")))
				.andExpect(status().isOk)
				.andExpect(jsonPath("$[0].name").value("Shopping List"))
	}

	@Test
	fun thatTaskslistsCanBeCreatedAndRetrieved() {
		mvc.perform(post("/tasklists")
				.content("""{"name": "My First Tasklist"}""")
				.contentType(APPLICATION_JSON_UTF8)
				.with(user("test1@localhost")))
				.andExpect(status().isCreated)

		mvc.perform(get("/tasklists")
				.with(user("test1@localhost")))
				.andExpect(status().isOk)
				.andExpect(jsonPath("$[0].name").value("My First Tasklist"))
	}

	@Test
	fun thatOnlyMyTasklistsAreRetrieved() {
		repository.save(Tasklist("1", "My Tasklist", "owner@localhost", emptyList(), emptyList()))
		repository.save(Tasklist("2", "Others Tasklist", "other@localhost", emptyList(), emptyList()))
		repository.save(Tasklist("3", "My other Tasklist", "owner@localhost", emptyList(), emptyList()))

		mvc.perform(get("/tasklists")
				.with(user("owner@localhost")))
				.andExpect(status().isOk)
				.andExpect(jsonPath("$[1]").exists())
				.andExpect(jsonPath("$[2]").doesNotExist()) // workaround for $("$"), hasSize(2)
	}

}
