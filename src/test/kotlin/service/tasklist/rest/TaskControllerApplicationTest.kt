package service.tasklist.rest

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import service.tasklist.domain.Task
import service.tasklist.domain.Tasklist
import service.tasklist.domain.TasklistRepository

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class TaskControllerApplicationTest {

    @Autowired
    lateinit var mvc: MockMvc

    @Autowired
    lateinit var repository: TasklistRepository

    @Test
    fun thatTaskCanBeAdded() {
        repository.save(Tasklist("1", "My Tasklist", "owner@localhost", emptyList(), emptyList()))

        mvc.perform(MockMvcRequestBuilders.post("/tasklists/1/tasks")
                .content("""{"name": "My First Task"}""")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .with(SecurityMockMvcRequestPostProcessors.user("owner@localhost")))
                .andExpect(MockMvcResultMatchers.status().isCreated)

        mvc.perform(MockMvcRequestBuilders.post("/tasklists/1/tasks")
                .content("""{"name": "My Second Task", "description": "This is the description"}""")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .with(SecurityMockMvcRequestPostProcessors.user("owner@localhost")))
                .andExpect(MockMvcResultMatchers.status().isCreated)

        mvc.perform(MockMvcRequestBuilders.get("/tasklists/1")
                .with(SecurityMockMvcRequestPostProcessors.user("owner@localhost")))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(jsonPath("$.tasks[0].name").value("My First Task"))
                .andExpect(jsonPath("$.tasks[1].name").value("My Second Task"))
    }

    @Test
    fun thatTaskCanBeUpdated() {
        repository.save(Tasklist("1", "My Tasklist", "owner@localhost", arrayListOf(
                Task("1", "name1", null, null),
                Task("2", "name2", null, null)
        ), emptyList()))

        mvc.perform(MockMvcRequestBuilders.put("/tasklists/1/tasks/2")
                .content("""{"name": "Updated Task"}""")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .with(SecurityMockMvcRequestPostProcessors.user("owner@localhost")))
                .andExpect(MockMvcResultMatchers.status().isOk)

        mvc.perform(MockMvcRequestBuilders.get("/tasklists/1")
                .with(SecurityMockMvcRequestPostProcessors.user("owner@localhost")))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(jsonPath("$.tasks[0].name").value("name1"))
                .andExpect(jsonPath("$.tasks[1].name").value("Updated Task"))
    }

    @Test
    fun thatTaskCanBeDeleted() {
        repository.save(Tasklist("1", "My Tasklist", "owner@localhost", arrayListOf(
                Task("1", "name1", null, null),
                Task("2", "name2", null, null)
        ), emptyList()))

        mvc.perform(MockMvcRequestBuilders.delete("/tasklists/1/tasks/1")
                .with(SecurityMockMvcRequestPostProcessors.user("owner@localhost")))
                .andExpect(MockMvcResultMatchers.status().isOk)

        mvc.perform(MockMvcRequestBuilders.get("/tasklists/1")
                .with(SecurityMockMvcRequestPostProcessors.user("owner@localhost")))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(jsonPath("$.tasks[0].taskId").value("2"))
                .andExpect(jsonPath("$.tasks[1]").doesNotExist())
    }
}