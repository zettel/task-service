package service.tasklist

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class SecurityTests {

	@Autowired
	lateinit var mvc: MockMvc

	@Test
	fun shouldDenyWithExpiredToken() {
		mvc.perform(MockMvcRequestBuilders.get("/tasklists")
				.header("Authorization", "Bearer " + "eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJqb2NoZW5jaHJpc3QrMUBnbWFpbC5jb20iLCJ1c2VyX25hbWUiOiJqb2NoZW5jaHJpc3QrMUBnbWFpbC5jb20iLCJleHAiOjE0OTk4NTczNzMsImlzcyI6ImF1dGhsaW5rIn0.cU-GwfUhWqIThdWx5EmVJspGQbGLWsZzBvC5-_xjD9CD5R4ehN5Qh0OPue_rOPzYmebmw4Kyi-342slg_6So2h906ssQlNx32DTevxxHooxRxIi5YrLFEW4jlY3RAenehwEdbVFo2HLJ82ZI0rmLct1cgsCfBusqeyGYUjKXpnM1VK-mnqzSvurIx6Iz948ktegJvIFtRNWmzvBx51qQfnFFyToJUzIZpdlPfQ74JrQ1MSWOhCoZCj3oh1yf6Bge8SU3LmQPdelmgyYTAFHpyA9ZXHEc1tCDfxANj4pFs2TA2kircWl9fd9tOdhXarf90g0vaHM62NNz6i3gn9CEAA"))
				.andExpect(MockMvcResultMatchers.status().isUnauthorized)
	}

	@Test
	fun shouldDenyAnonymousCalls() {
		mvc.perform(MockMvcRequestBuilders.get("/tasklists"))
				.andExpect(MockMvcResultMatchers.status().isUnauthorized)
	}

}
