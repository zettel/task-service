package service.tasklist.domain

public interface TasklistRepositoryCustom {
    fun addTask(tasklistId: String, task: Task): Tasklist?
    fun updateTask(tasklistId: String, task: Task)
    fun deleteTask(tasklistId: String, taskId: String)
}
