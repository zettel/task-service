package service.tasklist.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDate

@Document(collection = "tasklist")
class Tasklist(
        @Id val tasklistId: String,
        var name: String,
        @Indexed
        var owner: String,
        var tasks: List<Task>,
        var sharedWith: List<String>
)

class Task(
        var taskId: String,
        var name: String,
        var description: String?,
        var dueDate: LocalDate?
)