package service.tasklist.domain

import com.mongodb.BasicDBObject
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update

class TasklistRepositoryImpl (
        val mongo: MongoTemplate) : TasklistRepositoryCustom {

    override fun addTask(tasklistId: String, task: Task): Tasklist? {
        val query = Query.query(Criteria.where("tasklistId").`is`(tasklistId))
        val update = Update().addToSet("tasks").value(task)
        return mongo.findAndModify(query, update, Tasklist::class.java)
    }

    override fun updateTask(tasklistId: String, task: Task) {
        val query = Query.query(Criteria.where("tasklistId").`is`(tasklistId).and("tasks.taskId").`is`(task.taskId))
        val update = Update().set("tasks.$", task)
        mongo.findAndModify(query, update, Tasklist::class.java)
    }

    override fun deleteTask(tasklistId: String, taskId: String) {
        val query = Query.query(Criteria.where("tasklistId").`is`(tasklistId))
        val update = Update().pull("tasks", BasicDBObject("taskId", taskId))
        mongo.findAndModify(query, update, Tasklist::class.java)
    }
}