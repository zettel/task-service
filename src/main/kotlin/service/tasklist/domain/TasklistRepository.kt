package service.tasklist.domain

import org.springframework.data.mongodb.repository.MongoRepository

interface TasklistRepository : MongoRepository<Tasklist, String>, TasklistRepositoryCustom {
    fun findAllByOwner(owner: String) : List<Tasklist>
    fun findByTasklistIdAndOwner(id: String, owner: String) : Tasklist?
    fun deleteByTasklistIdAndOwner(id: String, owner: String)
}