package service.tasklist.rest

import org.springframework.http.HttpStatus.CREATED
import org.springframework.http.HttpStatus.UNAUTHORIZED
import org.springframework.web.bind.annotation.*
import service.tasklist.domain.Task
import service.tasklist.domain.Tasklist
import service.tasklist.domain.TasklistRepository
import java.security.Principal
import java.time.LocalDate
import java.util.*

@RestController
@RequestMapping("/tasklists/{tasklistId}/tasks")
@CrossOrigin
class TaskController (
        val repository: TasklistRepository
){

    @GetMapping
    fun getTasks(@PathVariable tasklistId: String, principal: Principal) : Tasklist? {
        return repository.findByTasklistIdAndOwner(tasklistId, principal.name)
    }

    @PostMapping
    @ResponseStatus(CREATED)
    fun createTask(@PathVariable tasklistId: String,
                   @RequestBody body: CreateTask,
                   principal: Principal) {
        checkPermission(tasklistId, principal)
        val task = Task(UUID.randomUUID().toString(), body.name, body.description, null)
        repository.addTask(tasklistId, task)
    }

    @PutMapping("/{taskId}")
    fun updateTask(@PathVariable tasklistId: String,
                   @PathVariable taskId: String,
                   @RequestBody body: UpdateTask,
                   principal: Principal) {
        checkPermission(tasklistId, principal)
        val task = Task(taskId, body.name, body.description, body.dueDate)
        repository.updateTask(tasklistId, task)

    }

    @DeleteMapping("/{taskId}")
    fun deleteTask(@PathVariable tasklistId: String,
                   @PathVariable taskId: String,
                   principal: Principal) {
        checkPermission(tasklistId, principal)
        repository.deleteTask(tasklistId, taskId)
    }

    fun checkPermission(tasklistId: String, principal: Principal) {
        if(repository.findByTasklistIdAndOwner(tasklistId, principal.name) == null) {
            throw UnauthorizedException()
        }
    }
}


data class CreateTask (
        val name: String,
        val description: String?
)

data class UpdateTask (
        val name: String,
        val description: String?,
        val dueDate: LocalDate?
)

@ResponseStatus(UNAUTHORIZED)
class UnauthorizedException : RuntimeException("Unauthorized access")