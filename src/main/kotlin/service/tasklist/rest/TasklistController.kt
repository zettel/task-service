package service.tasklist.rest

import org.springframework.http.HttpStatus.CREATED
import org.springframework.web.bind.annotation.*
import service.tasklist.domain.Task
import service.tasklist.domain.Tasklist
import service.tasklist.domain.TasklistRepository
import java.security.Principal
import java.util.*

@RestController
@RequestMapping("/tasklists")
@CrossOrigin
class TasklistController (
        val repository: TasklistRepository
){

    @GetMapping
    fun getMyTasklists(principal: Principal) : List<TasklistResponse> {
        var tasklists = repository.findAllByOwner(principal.name)

        if(tasklists.isEmpty()) {
            tasklists = repository.save(initialTasklists(principal.name))
        }

        return tasklists
                .map { it.toResponse() }
    }

    private fun initialTasklists(owner: String) : List<Tasklist> {
       return listOf(
               Tasklist(UUID.randomUUID().toString(),
                "Shopping List",
                owner,
                listOf(Task(UUID.randomUUID().toString(), "Milk", null, null)),
                emptyList())
       )

    }

    @GetMapping("/{tasklistId}")
    fun getMyTasklists(@PathVariable tasklistId: String,
                       principal: Principal) : TasklistResponse {
        val tasklist = repository.findByTasklistIdAndOwner(tasklistId, principal.name)
        if(tasklist == null) {
            throw RuntimeException("Could not find tasklist")
        } else {
            return tasklist.toResponse()
        }
    }


    @PostMapping
    @ResponseStatus(CREATED)
    fun createTasklist(@RequestBody body: CreateTasklist,
                       principal: Principal) {
        repository.save(Tasklist(UUID.randomUUID().toString(), body.name, principal.name, emptyList(), emptyList()))
    }

    @DeleteMapping("/{tasklistId}")
    fun deleteTasklist(@PathVariable tasklistId: String,
                       principal: Principal) {
        repository.deleteByTasklistIdAndOwner(tasklistId, principal.name)
    }


}

data class TasklistResponse (
        val id: String,
        val name: String,
        val tasks: List<Task>
)

fun Tasklist.toResponse(): TasklistResponse {
    return TasklistResponse(this.tasklistId, this.name, this.tasks)
}

data class CreateTasklist (
        val name: String
)