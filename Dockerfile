FROM anapsix/alpine-java:8
VOLUME /tmp
ADD build/libs/task-service.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]